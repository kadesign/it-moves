const cnv = document.querySelector('#move')

if (cnv.getContext) {
    const ctx = cnv.getContext('2d')
    const msg = document.querySelector('#wow')

    // These can be changed
    const cnvW = 650    // Canvas width
    const cnvH = 450    // Canvas height
    const w = 30        // Width of unit
    const h = 30        // Height of unit
    const animLen = 500 // Animation length

    // Don't touch them (please)!
    let posX = (cnvW - w) / 2      // Current position of unit (axis X)
    let posY = (cnvH - h) / 2      // Current position of unit (axis Y)
    let spd = 0         // Movement speed
    let spdUp = false   // Gas button is pressed
    let mvmt = false    // Unit moves

    // These too! (it's all about animations)
    let fadeInt
    let fadeTO

    cnv.setAttribute('width', cnvW)
    cnv.setAttribute('height', cnvH)

    const getMsg = function () {
        let index = Math.floor(Math.random() * 7)
        return msgLib[index]
    }

    const updateProps = function () {
        const qPosX = document.querySelector('#posX')
        const qPosY = document.querySelector('#posY')
        const qSpd = document.querySelector('#spd')

        qPosX.innerHTML = posX
        qPosY.innerHTML = posY
        qSpd.innerHTML = spd
    }

    const move = function (x, y) {
        let posXnew
        let posYnew

        if (!mvmt) {
            mvmt = true
            msg.innerHTML = getMsg()
            msg.style.opacity = 1

            if (fadeInt) {
                clearInterval(fadeInt)
                clearTimeout(fadeTO)
            }

            fadeInt = setInterval(() => {
                msg.style.opacity -= 0.01
            }, animLen / 100)
            fadeTO = setTimeout(() => clearInterval(fadeInt), animLen)
        }

        spd = spdUp ? 5 : 2

        if (posX + x < 0) {
            posXnew = 0
            posYnew = posY + y
        } else if (posY + y < 0) {
            posXnew = posX + x
            posYnew = 0
        } else if (posX + x > (cnvW - w)) {
            posXnew = cnvW - w
            posYnew = posY + y
        } else if (posY + y > (cnvH - h)) {
            posXnew = posX + x
            posYnew = cnvH - h
        } else {
            posXnew = posX + x
            posYnew = posY + y
        }

        posX = posXnew
        posY = posYnew
        ctx.clearRect(0, 0, cnvW, cnvH)
        ctx.fillRect(posX, posY, w, h)
        updateProps()
    }

    const stop = function () {
        spd = 0
        mvmt = false
        updateProps()
    }

    ctx.fillStyle = 'rgb(107, 209, 98)'
    ctx.fillRect(posX, posY, w, h)

    updateProps()

    // Bind controls
    kd.UP.down(() => move(0, -spd))
    kd.UP.up(() => stop())
    kd.DOWN.down(() => move(0, spd))
    kd.DOWN.up(() => stop())
    kd.LEFT.down(() => move(-spd, 0))
    kd.LEFT.up(() => stop())
    kd.RIGHT.down(() => move(spd, 0))
    kd.RIGHT.up(() => stop())
    kd.SHIFT.down(() => spdUp = true)
    kd.SHIFT.up(() => spdUp = false)

    kd.run(() => kd.tick());
} else {
    document.querySelector('#wow').remove()
    document.querySelector('.props').remove()
    document.querySelector('.controls').remove()
}

